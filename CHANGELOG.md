# OKToFly Changelog
This changelog lists the changes in OKToFly from version-to-version.

## Version 1.1.0 - Released on March 17, 2020
Version 1.1.0 of OKToFly includes some fixes and new features to improve the OKToFly experience.

**New features/removals:**
* Adds in a pretty loader.
* Adds in update checking. It is on by default.
* UI refinements, including better readability.
* Proper import catching for Pytz since not all Python installations include this library.
* Refined the about section of OKToFly

**Bug fixes:**
* Fixed a bug that could have prevented Pro Mode from being enabled.
* Other misc. bug fixes.

**Other changes:**
* New installations of OKToFly will now have the condensed warnings option set to False by default.

## Version 1.0.0 - Released on March 16, 2020
It's been a while! I came back to make some fixes to OKToFly. This release combines the in development 0.4.0 beta and the 1.0.0 release today.

**New features/removals:**
* Adds support for doing some alert calculations on hourly data - depending on expiration time
* Adds better support for score explanations for alert subtractions
* Adds support for local timezones in the (at time) dialogue
* Adds an about screen
* Adds a current page for re-viewing the current data
* Hourly now has warnings when condensed warnings are off.

**Bug fixes:**
* Fixed a known issue with Dark Sky's alert intensity field improperly syncing up with the alert title.
* Fixed a bug where there would be improper formatting on the hourly & current screens with the current time - If the hour was 10, 11, or 12, there would be no space between the text and the time.

OKToFly development is considered done, for the most part. I may take some time to refine documentation, add in update checking and pretty loader support, and other misc things soon.

## Version 0.3.0 beta - Released on February 5, 2019
**New features/removals:**
* Adds support for hourly data
* Adds support for a pro-mode
* Adds support for condensing warning messages onto one-line
* Adds a powered by Dark Sky message
* Adds support to detect advisories, watches and warnings and factor that into the score

**Bug fixes:**
* Fixes a bug with the wind equation that set the upper limit much too high

**Known issues:**
* You may see a discrepancy with the warning titles and how many points are subtracted from the final score, this is because sometimes the Dark Sky intensity field doesn't sync up with the alert title. Better support is coming in v0.4.0 beta (CLI edition)

## Version 0.2.0 beta - Released on January 8, 2019
**New features/removals:**
* Adds support for config-defined API keys
* Adds emojis for the checklist
* Tweaks the temperature scale: Before, between 32F and 50F would bring the score from 0.25 to 1.0. To avoid this, the range for 0.25 to 1.0 is now 15F to 50F.
* Adds error checking for non-standard libraries
* Adds the location you've looked up the info for on the main screen
* Adds detailed explanations of each data category - Scrapped

**Bug fixes:**
* No bug fixes...just yet!

## Version 0.1.0 beta - Released on January 4, 2019
**New features/removals:**
* Initial release
* Basic support for checking local conditions
* Support for warning messages depending on certain conditions

**Bug fixes:**
* None in this release