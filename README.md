# Welcome to OKToFly (v1.1.0)

A simple Python program that checks: Is it OK to fly your drone?

# Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

OKToFly Spaghet-o-meter: **10/10**

Without a doubt, OKToFly is spaghetti code. Although there is modularity in the form of a config file and functions - the main issue lies in the logic programming. This, without a doubt, should have been reserved to a separate function that took an input, and returned an output. The exact same thing can be said for the emoji calculations as well. This is compounded by the fact that the analysis code is copied for the forecast. The length of the oktofly.py file is also excessive at over 1,000 lines.

# About
I got a drone for Christmas 2018, and I wanted to create a simple tool that help checks if the weather conditions permit flying my drone.

OKToFly is also meant to be a project where I can get familiar with Qt programming, and the Dark Sky API that I'll use in upcoming projects.

# Disclaimer
Please note that this tool isn't a magical, AI-based system that can precisely tell you when you should be flying your drone.

Make sure that before you fly your drone, check the weather conditions by going outside. Yes, it's scary, but it'll help you judge for yourself if you want to be flying outside.

OKToFly is meant as a tool to help you determine if you should be flying your drone or not. There are cases where OKToFly will tell you that it's OK to fly, but winds outside will be much too high to safely fly.

By using this tool, you agree that you cannot hold me liable for any damages that occur from using this program, whether indirectly or directly.

# What does OKToFly do?
OKToFly gives you a summary of if you should be flying your drone outside.

Look up your current location, and you'll get generalized information as to if it's okay to fly your drone, take caution, or not fly at all.

You can also see hourly conditions for up to 6 hours out, perfect for seeing if the weather is good to fly your drone in a few hours.

# Setup
To use OKToFly, you'll need a few things:
* A computer
* An internet connection
* A computer that has Python 3 available
* A Dark Sky key, as OKToFly does not come with one.

Additionally, you'll also need to install a few extra libraries:
* Requests (use `pip3 install requests` in a command line to install)
* Geopy (use `pip3 install geopy` in a command line to install)
* Halo (use `pip3 install halo` in a command line to install)
* Pytz (use `pip3 install pytz` in a command line to install)

After that, you can either download a pre-packaged .zip, or use Git. If you'd just like to download a release, click on Tags, and download the latest version available.

For Git users, you can download by doing this:
```
git clone https://gitlab.com/o355/oktofly.git
cd oktofly
git checkout v1.1.0 (or whatever the latest release is)
```

If you'd like, you can also use the latest indev code:
```
git clone https://gitlab.com/o355/oktofly.git
cd oktofly
```

When you first run OKToFly, config values will be populated, and it should run. You will then need to enter in a Dark Sky API key, as OKToFly does not come with one.

# Features & development state
OKToFly is currently mostly done with development. It was a tool I made in 2019 and it serves it's purpose pretty well. It's a good idea to port over to the web, or maybe have a little fun with a Flask API some day for a website.
