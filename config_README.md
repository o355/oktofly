The configuration file for OKToFly controls a variety of usage options in OKToFly.

For OKToFly v1.0.0

## MAIN section

### apikey
This controls your Dark Sky API key. Necessary for operation.

Type: `str`

Defaults to: `''`

### condensedwarnings
This controls if on the summary screen warnings are shown as either:

```
⚠ There are 2 additional warnings. Enter w for more info.
```
(True)

or...

```
⚠ The current UV index is 7. Make sure to apply sunscreen often, and stay in shade if possible.

⚠ The current temperature is 109°F. Try to limit flight time to prevent drone overheating. 
  Make sure you stay hydrated, and limit your time outside to prevent heat stroke.
``` 
(False)

This also applies to the hourly section.

Type: `boolean`

Defaults to: `False` as of v1.1.0 and later, defaulted to `True` in v1.0.0 and earlier

### promode
This controls if the OK to fly summary message, and the associated emojis on the summary & hourly screens are changed to an algorithm more suited for drone professionals.

If you've been flying drones for a while, and have some additional experience with unfavorable situations, you can turn this on if you'd like to. If you're a newcomer to flying drones, keep this option disabled.

If you'd like to see how the algorithms change depending on if you have pro mode on or off, click [here](https://gitlab.com/o355/oktofly/wikis/Emoji-Algorithms).

Type: `boolean`

Defaults to: `False`

### checkforupdates
This controls if OKToFly will check for updates on startup. The update check, at most, adds 3 seconds to the startup time, and uses under 10 KB of data.

Type: `boolean`

Defaults to: `True`