# OKToFly Version 1.1.0
# Copyright (C) 2019 - 2020, o355

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Imports
import json
import sys
import configparser
from datetime import datetime

# Pulled from stack overflow for different key capture methods
print("----------------------")
try:
    from msvcrt import getch
except ImportError:
    def getch():
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            return sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old)

try:
    import pytz
except (ImportError, pytz):
    print("OKToFly needs to have pytz installed to work. Please install pytz before",
          "using OKToFly by running pip3 install pytz", sep="\n")
    print("----------------------")
    sys.exit()

try:
    import requests
except (ImportError, requests):
    print("OKToFly needs to have Requests installed to work. Please install Requests before",
          "using OKToFly by running pip3 install requests", sep='\n')
    print("----------------------")
    sys.exit()


try:
    from geopy import ArcGIS
    # PyCharm told me to add "ArcGIS" to the import exception for whatever reason
except (ImportError, ArcGIS):
    print("OKToFly needs to have Geopy installed to work. Please install Geopy before",
          "using OKToFly by running pip3 install geopy. If you already have Geopy installed,",
          "please update Geopy before using OKToFly by running pip3 install geopy --upgrade.", sep="\n")
    print("----------------------")
    sys.exit()

try:
    import geopy
except (ImportError, geopy):
    print("OKToFly needs to have Geopy installed to work. Please install Geopy before",
          "using OKToFly by running pip3 install geopy", sep="\n")
    print("----------------------")
    sys.exit()

try:
    from halo import Halo
except ImportError:
    print("OKToFly needs to have Halo installed to work. Please install Halo before",
          "using OKToFly by running pip3 install halo.", sep="\n")
    print("----------------------")
    sys.exit()

# Config (not yet implemented)
config = configparser.ConfigParser()
config.read('config.ini')

try:
    apikey = config['SETTINGS']['apikey']
    if apikey == "":
        print("An API key was not defined. You will need a Dark Sky API key",
              "to use OKToFly. Please define one in config.ini, under the",
              "SETTINGS section.", sep="\n")
        print("----------------------")
        sys.exit()
except KeyError:
    # Check for missing keys
    if "SETTINGS" not in config.sections():
        config.add_section('SETTINGS')

    config['SETTINGS']['apikey'] = ""

    with open('config.ini', 'w') as configfile:
        config.write(configfile)

    print("An API key was not defined. You will need a Dark Sky API key",
          "to use OKToFly. Please define one in config.ini, under the",
          "SETTINGS section.", sep="\n")
    print("----------------------")
    sys.exit()

try:
    condensedwarnings = config.getboolean('SETTINGS', 'condensedwarnings')
except configparser.NoOptionError:
    config['SETTINGS']['condensedwarnings'] = "False"

    condensedwarnings = False

    with open('config.ini', 'w') as configfile:
        config.write(configfile)
except ValueError:
    print("Your condensed warnings on summary option in the config file is not either True or False.",
          "Please make sure it's set to either True or False. Defaulting to True.", sep="\n")
    print("----------------------")
    condensedwarnings = False

try:
    promode = config.getboolean('SETTINGS', 'promode')
except configparser.NoOptionError:
    config['SETTINGS']['promode'] = "False"

    promode = False

    with open('config.ini', 'w') as configfile:
        config.write(configfile)
except ValueError:
    print("Your pro mode option in the config file is not either True or False.",
          "Please make sure it's set to either True or False. Defaulting to False.", sep="\n")
    print("----------------------")
    promode = False

try:
    checkforupdates = config.getboolean('SETTINGS', 'checkforupdates')
except configparser.NoOptionError:
    config['SETTINGS']['checkforupdates'] = "True"
    checkforupdates = True

    with open('config.ini', 'w') as configfile:
        config.write(configfile)
except ValueError:
    print("Your check for updates option in the config file is not either True or False.",
          "Please make sure it's set to either True or False. Defaulting to False.", sep="\n")
    print("----------------------")
    checkforupdates = False

# Define about variables
version = "1.1.0"
buildnumber = 110
releasedate = "March 17, 2020"
releasetype = "Release"

# Define geocoder
geocoder = ArcGIS()
spinner = Halo(text='Loading OKToFly...', spinner='line')
spinner.start()

def checkforupdates():
    try:
        updatedata = requests.get("https://gitlab.com/o355/oktofly/raw/master/updater/versioncheck.json", timeout=5)
        updatedata = updatedata.json()
        buildnumber_new = int(updatedata['updater']['latestbuild'])
        if buildnumber_new > buildnumber:
            spinner.stop()
            print("A new version of OKToFly is available! You have version " + version + ", and the latest is version " + updatedata['updater']['latestversion'])
            print("You can download version " + updatedata['updater']['latestversion'] + " at " + updatedata['updater']['latesturl'])
            print("----------------------")
        else:
            spinner.stop()
    except requests.exceptions.RequestException:
        spinner.stop()
        pass
    except requests.exceptions.ConnectTimeout:
        spinner.stop()
        pass


if checkforupdates:
    checkforupdates()
else:
    spinner.stop()
print("Welcome to OKToFly, powered by Dark Sky.")
print("Please enter a location to look up conditions for below.")
locinput = input("Input here: ")

spinner.start("Fetching weather data...")
try:
    location = geocoder.geocode(locinput)
except geopy.exc.GeocoderServiceError:
    spinner.fail("An error occurred when geocoding your location. Try again later.")
    sys.exit()

try:
    latstr = str(location.latitude)
    lonstr = str(location.longitude)
except AttributeError:
    spinner.fail("No latitude/longitude found for location entered. Try another location.")
    sys.exit()

weatherurl = 'https://api.darksky.net/forecast/' + apikey + '/' + latstr + ',' + lonstr

try:
    weather_data = requests.get(weatherurl)
except:
    spinner.fail("Failed to fetch weather data. Check your connection & API key, then try again.")
    sys.exit()

weather_json = json.loads(weather_data.text)

# Begin to process weather data
temperature = weather_json['currently']['temperature']
precip_intensity = weather_json['currently']['precipIntensity']
if precip_intensity > 0:
    precip_type = weather_json['currently']['precipType']
else:
    precip_type = "none"
wind_speed = weather_json['currently']['windSpeed']
wind_gust = weather_json['currently']['windGust']
visibility = weather_json['currently']['visibility']
conditions = weather_json['currently']['summary']
uv_index = weather_json['currently']['uvIndex']
try:
    nearest_storm_distance = weather_json['currently']['nearestStormDistance']
except KeyError:
    nearest_storm_distance = 999
humidity = weather_json['currently']['humidity']
cloud_cover = weather_json['currently']['cloudCover']
timezone = weather_json['timezone']
tz = pytz.timezone(timezone)

# Run the temperature algorithm
if temperature <= 0:
    temperature_score = 0.0
elif 0 < temperature <= 15:
    # Equation: 0.016666666666666666x (where x is temperature in F)
    temperature_score = 0.016666666666666666 * temperature
elif 15 < temperature <= 50:
    # Equation: 0.02142857142857143(x - 15) + 0.25 (where x is temperature in F)
    temperature_score = 0.02142857142857143 * (temperature - 15) + 0.25
elif 50 < temperature <= 80:
    temperature_score = 1.0
elif 80 < temperature <= 95:
    # Equation: -0.05 * (x - 80) + 1 (where x is temperature)
    temperature_score = -0.05 * (temperature - 80) + 1
elif 95 < temperature <= 110:
    # Equation: -0.016666666666666666(x - 95) + 0.25 (where x is temperature in F)
    temperature_score = -0.016666666666666666 * (temperature - 95) + 0.25
elif temperature > 110:
    temperature_score = 0.0
else:
    temperature_score = 0.0

# Run the wind algorithm
if wind_speed < 10:
    wind_score = 2.0
elif 10 <= wind_speed <= 20:
    # Equation: -0.20(x - 10) + 2.0 (where x is wind speed in MPH)
    wind_score = -0.20 * (wind_speed - 10) + 2.0
elif wind_speed > 20:
    wind_score = 0.0
else:
    wind_score = 0.0

# Run the wind gust algorithm
wind_gust_difference = wind_gust - wind_speed
if wind_gust_difference < 7:
    wind_gust_score = 1.0
elif 7 <= wind_gust_difference <= 15:
    # Equation: -0.125(x - 7) + 1.0 (where x is wind gust difference in MPH)
    wind_gust_score = -0.125 * (wind_gust_difference - 7) + 1.0
elif wind_gust_difference > 15:
    wind_gust_score = 0.0
else:
    wind_gust_score = 0.0

# Calculate precipitation score based on in/hr precip rate.

if precip_intensity == 0.0:
    precip_score = 5.0
elif 0.0 < precip_intensity <= 0.05:
    if precip_type == "rain":
        # Equation: -80.0 * (x - 0.05) + 1 (where x is raw precip rate)
        precip_score = -80.0 * (precip_intensity - 0.05) + 1
    elif precip_type == "snow" or precip_type == "sleet":
        # Equation: -100.0 * (x - 0.05) (where x is raw precip rate)
        precip_score = -100.0 * (precip_intensity - 0.05)
        # Fix for a negative -0.0, make it 0.0
        if precip_score == -0.0:
            precip_score = 0.0
    else:
        # Equation: -80.0 * (x - 0.05) + 1 (where x is raw precip rate)
        precip_score = -80.0 * (precip_intensity - 0.05) + 1
elif 0.05 < precip_intensity <= 0.075:
    if precip_type == "rain":
        # Equation: -40.0 * (x - 0.075) (where x is raw precip rate)
        precip_score = -40.0 * (precip_intensity - 0.075)
        # Fix for a -0.0 score, make it 0.0
        if precip_score == -0.0:
            precip_score = 0.0
    elif precip_type == "snow" or precip_type == "sleet":
        precip_score = 0.0
    else:
        # Equation: -40.0 * (x - 0.075) (where x is raw precip rate)
        precip_score = -40.0 * (precip_intensity - 0.075)
        # Fix for a -0.0 score, make it 0.0
        if precip_score == -0.0:
            precip_score = 0.0
elif precip_intensity > 0.075:
    precip_score = 0.0
else:
    precip_score = 0.0

# Calculate visibility score
if visibility > 5:
    visibility_score = 1.0
elif 1 <= visibility <= 5:
    # Equation: -0.25(4 - x) + 0.75 (where x is visibility in miles)
    visibility_score = -0.25 * (4 - visibility) + 0.75
elif visibility < 1:
    visibility_score = 0.0
else:
    visibility_score = 0.0


temperature_score = round(temperature_score, 2)
wind_score = round(wind_score, 2)
wind_gust_score = round(wind_gust_score, 2)
precip_score = round(precip_score, 2)
visibility_score = round(visibility_score, 2)
wind_gust_difference = round(wind_gust_difference, 2)

final_score = temperature_score + wind_score + wind_gust_score + precip_score + visibility_score
final_score = round(final_score, 2)

# Do some emoji calculations
good_emoji = "✔"
caution_emoji = "⚠"
bad_emoji = "✖"

# Temperature calculations
if promode is True:
    if 0.49 <= temperature_score <= 1.0:
        temperature_emoji = good_emoji
    elif 0.21 <= temperature_score < 0.49:
        temperature_emoji = caution_emoji
    elif 0.0 <= temperature_score < 0.21:
        temperature_emoji = bad_emoji
    else:
        temperature_emoji = "?"
else:
    if 0.61 <= temperature_score <= 1.0:
        temperature_emoji = good_emoji
    elif 0.25 <= temperature_score < 0.61:
        temperature_emoji = caution_emoji
    elif 0.0 <= temperature_score < 0.25:
        temperature_emoji = bad_emoji
    else:
        temperature_emoji = "?"


# Wind calculations
if promode is True:
    if 1.3 <= wind_score <= 2.0:
        wind_emoji = good_emoji
    elif 0.5 <= wind_score < 1.3:
        wind_emoji = caution_emoji
    elif 0.0 <= wind_score < 0.5:
        wind_emoji = bad_emoji
    else:
        wind_emoji = "?"
else:
    if 1.6 <= wind_score <= 2.0:
        wind_emoji = good_emoji
    elif 0.8 <= wind_score < 1.6:
        wind_emoji = caution_emoji
    elif 0.0 <= wind_score < 0.8:
        wind_emoji = bad_emoji
    else:
        wind_emoji = "?"


# Wind Gust calculations
if promode is True:
    if 0.55 <= wind_gust_score <= 1.0:
        wind_gust_emoji = good_emoji
    elif 0.28 <= wind_gust_score < 0.55:
        wind_gust_emoji = caution_emoji
    elif 0.0 <= wind_gust_score < 0.28:
        wind_gust_emoji = bad_emoji
    else:
        wind_gust_emoji = "?"
else:
    if 0.8 <= wind_gust_score <= 1.0:
        wind_gust_emoji = good_emoji
    elif 0.4 <= wind_gust_score < 0.8:
        wind_gust_emoji = caution_emoji
    elif 0.0 <= wind_gust_score < 0.4:
        wind_gust_emoji = bad_emoji
    else:
        wind_gust_emoji = "?"

# Precip calculations
if promode is True:
    if 4.50 <= precip_score <= 5.0:
        precip_emoji = good_emoji
    elif 3.3 <= precip_score < 4.50:
        precip_emoji = caution_emoji
    elif 0.0 <= precip_score < 3.3:
        precip_emoji = bad_emoji
    else:
        precip_emoji = "?"
else:
    if 5.0 <= precip_score <= 5.0:
        precip_emoji = good_emoji
    elif 3.5 <= precip_score < 5.0:
        precip_emoji = caution_emoji
    elif 0.0 <= precip_score < 3.5:
        precip_emoji = bad_emoji
    else:
        precip_emoji = "?"

# Visibility calculations
if promode is True:
    if 0.55 <= visibility_score <= 1.0:
        visibility_emoji = good_emoji
    elif 0.25 <= visibility_score < 0.55:
        visibility_emoji = caution_emoji
    elif 0.0 <= visibility_score < 0.25:
        visibility_emoji = bad_emoji
    else:
        visibility_emoji = "?"
else:
    if 0.75 <= visibility_score <= 1.0:
        visibility_emoji = good_emoji
    elif 0.5 <= visibility_score < 0.75:
        visibility_emoji = caution_emoji
    elif 0.0 <= visibility_score < 0.5:
        visibility_emoji = bad_emoji
    else:
        visibility_emoji = "?"

# Calculate how much the score gets subtracted because of alerts
# Sometimes, a region is on the border of two adjacent alerts, so we store the name of an alert, and if the alert has already
# been pulled up, continue along.

# Promode reduces the impact of the score - 0.35 for advisory, 0.75 for watch, and 1.5 for warning.
alertnames = []
try:
    for alert in weather_json['alerts']:
        severity = alert['severity']
        title = alert['title']
        if title in alertnames:
            continue
        else:
            alertnames.append(title)

        if promode is True:
            if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                final_score = final_score - 0.35
            elif title.lower().find("watch") == 0:
                final_score = final_score - 0.75
            elif title.lower().find("warning") == 0:
                final_score = final_score - 1.5
            elif severity == "advisory":
                final_score = final_score - 0.35
            elif severity == "watch":
                final_score = final_score - 0.75
            elif severity == "warning":
                final_score = final_score - 1.5
            else:
                continue
        else:
            if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                final_score = final_score - 0.5
            elif title.lower().find("watch") == 0:
                final_score = final_score - 1.0
            elif title.lower().find("warning") == 0:
                final_score = final_score - 2.0
            elif severity == "advisory":
                final_score = final_score - 0.5
            elif severity == "watch":
                final_score = final_score - 1.0
            elif severity == "warning":
                final_score = final_score - 2.0
            else:
                continue

except KeyError:
    keeping = "yes"

# If the score is below 0.0, bottom it out at 0.0.

if final_score < 0.0:
    final_score = 0.0

# Round out the final score again
final_score = round(final_score, 2)

# Calculate amount of warnings
warnings = 0
if uv_index >= 6.0:
    warnings = warnings + 1

if humidity >= 0.90:
    warnings = warnings + 1

if temperature <= 20:
    warnings = warnings + 1

if temperature >= 100:
    warnings = warnings + 1

if cloud_cover >= 0.95:
    warnings = warnings + 1

local_warnings = warnings

time = weather_json['currently']['time']

spinner.stop()
print("----------------------")
print("Here's the score for %s at %s:" % (location, datetime.fromtimestamp(time, tz).strftime("%-l:%M %p %Z")))

if promode is True:
    if 7.5 <= final_score <= 10.0:
        print(good_emoji + " It's OK to fly!")
    elif 5.5 <= final_score < 7.5:
        print(caution_emoji + " It's OK to fly, but take caution!")
    elif 4.5 <= final_score < 5.5:
        print(caution_emoji + " It's OK to fly, but take extreme caution!")
    elif 0.0 <= final_score < 4.5:
        print(bad_emoji + " It's not OK to fly.")
    else:
        print("? We had trouble calculating your score.")
else:
    if 8.5 <= final_score <= 10.0:
        print(good_emoji + " It's OK to fly!")
    elif 7.0 <= final_score < 8.5:
        print(caution_emoji + " It's OK to fly, but take caution!")
    elif 6.0 <= final_score < 7.0:
        print(caution_emoji + " It's OK to fly, but take extreme caution!")
    elif 0.0 <= final_score < 6.0:
        print(bad_emoji + " It's not OK to fly.")
    else:
        print("? We had trouble calculating your score.")

print("  Overall score: %s/10" % final_score)
if promode is True:
    print("  Pro mode is enabled.")
print("")
alertnames = []
try:
    for alert in weather_json['alerts']:
        severity = alert['severity']
        title = alert['title']
        if title in alertnames:
            continue
        else:
            alertnames.append(title)

        if promode is True:
            if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                print("⚠ There's a %s in effect for your area. 0.35 points subtracted from the total score." % title)
            elif title.lower().find("watch") == 0:
                print("⚠ There's a %s in effect for your area. 0.75 points subtracted from the total score." % title)
            elif title.lower().find("warning") == 0:
                print("⚠ There's a %s in effect for your area. 1.5 points subtracted from the total score." % title)
            elif severity == "advisory":
                print("⚠ There's a %s in effect for your area. 0.35 points subtracted from the total score." % title)
            elif severity == "watch":
                print("⚠ There's a %s in effect for your area. 0.75 points subtracted from the total score." % title)
            elif severity == "warning":
                print("⚠ There's a %s in effect for your area. 1.5 points subtracted from the total score." % title)
            else:
                continue
        else:
            if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                print("⚠ There's a %s in effect for your area. 0.5 points subtracted from the total score." % title)
            elif title.lower().find("watch") == 0:
                print("⚠ There's a %s in effect for your area. 1.0 points subtracted from the total score." % title)
            elif title.lower().find("warning") == 0:
                print("⚠ There's a %s in effect for your area. 2.0 points subtracted from the total score." % title)
            elif severity == "advisory":
                print("⚠ There's a %s in effect for your area. 0.5 points subtracted from the total score." % title)
            elif severity == "watch":
                print("⚠ There's a %s in effect for your area. 1.0 points subtracted from the total score." % title)
            elif severity == "warning":
                print("⚠ There's a %s in effect for your area. 2.0 points subtracted from the total score." % title)
            else:
                continue

except KeyError:
    keepgoing = "yes"

print(temperature_emoji + " Temperature score: %s/1.0 (Temp: %s°F)" % (temperature_score, temperature))
print(wind_emoji + " Wind score: %s/2.0 (Wind speed: %s MPH)" % (wind_score, wind_speed))
print(wind_gust_emoji + " Wind gust score: %s/1.0 (Wind gust: %s MPH | Wind gust difference: %s MPH)" %
      (wind_gust_score, wind_gust, wind_gust_difference))
print(precip_emoji + " Precipitation score: %s/5.0 (Precip intensity: %s in/hr)" %
      (precip_score, precip_intensity))
print(visibility_emoji + " Visibility score: %s/1.0 (Visibility: %s miles)" % (visibility_score, visibility))
if condensedwarnings is True:
    if warnings == 1:
        print("⚠ There is %s additional warning. Enter w to get more info." % warnings)
    elif warnings >= 2:
        print("⚠ There are %s additional warnings. Enter w to get more info." % warnings)
else:
    if 6.0 <= uv_index <= 10.0:
        print("")
        print("⚠ The current UV index is %s. Make sure to apply sunscreen often, and stay in shade if possible." % uv_index)
    elif uv_index > 10.0:
        print("")
        print("⚠ The current UV index is %s. Take extreme caution when in direct sunlight. Make sure to apply" % uv_index,
              "  sunscreen often, and stay in shade if possible.", sep="\n")

    if humidity >= 0.90:
        print("")
        print("⚠ The humidity level is %s percent. Water vapor may collect on your drone," % (humidity * 100),
              "  which could affect internal components & flight performance. Take extra care flying.", sep="\n")

    if temperature <= 20:
        print("")
        print("⚠ The current temperature is %s°F. Before flying, hover your drone for a minute" % temperature,
              "  to let the battery warm up. Make sure you are wearing a hat & gloves, and stay aware of frostbite.", sep="\n")

    if temperature >= 100:
        print("")
        print("⚠ The current temperature is %s°F. Try to limit flight time to prevent drone overheating." % temperature,
              "  Make sure you stay hydrated, and limit your time outside to prevent heat stroke.", sep="\n")

    if cloud_cover >= 0.95:
        print("")
        print("⚠ The current cloud cover is %s percent. You may have trouble keeping sight of your drone." % (cloud_cover * 100),
              "  If you lose sight, fly back immediately. Additionally, photos & videos could come out poorly.", sep="\n")

print("----------------------")
while True:
    print("Enter h to view hourly info, c to view current info, w to view warnings, a for about, or any other key to exit.")
    try:
        charinput = getch()
        print("")
    except KeyboardInterrupt:
        sys.exit()
    except:
        try:
            charinput = input("Input here, then press enter: ")
        except KeyboardInterrupt:
            sys.exit()
    if charinput == "b'h'" or charinput == "h":
        for i in range(1,7):
            print("----------------------")
            time = weather_json['hourly']['data'][i]['time']
            temperature = weather_json['hourly']['data'][i]['temperature']
            precip_intensity = weather_json['hourly']['data'][i]['precipIntensity']
            if precip_intensity > 0:
                precip_type = weather_json['hourly']['data'][i]['precipType']
            else:
                precip_type = "none"
            wind_speed = weather_json['hourly']['data'][i]['windSpeed']
            wind_gust = weather_json['hourly']['data'][i]['windGust']
            visibility = weather_json['hourly']['data'][i]['visibility']
            conditions = weather_json['hourly']['data'][i]['summary']
            uv_index = weather_json['hourly']['data'][i]['uvIndex']

            humidity = weather_json['hourly']['data'][i]['humidity']
            cloud_cover = weather_json['hourly']['data'][i]['cloudCover']

            # Run the temperature algorithm
            if temperature <= 0:
                temperature_score = 0.0
            elif 0 < temperature <= 15:
                # Equation: 0.016666666666666666x (where x is temperature in F)
                temperature_score = 0.016666666666666666 * temperature
            elif 15 < temperature <= 50:
                # Equation: 0.02142857142857143(x - 15) + 0.25 (where x is temperature in F)
                temperature_score = 0.02142857142857143 * (temperature - 15) + 0.25
            elif 50 < temperature <= 80:
                temperature_score = 1.0
            elif 80 < temperature <= 95:
                # Equation: -0.05 * (x - 80) + 1 (where x is temperature)
                temperature_score = -0.05 * (temperature - 80) + 1
            elif 95 < temperature <= 110:
                # Equation: -0.016666666666666666(x - 95) + 0.25 (where x is temperature in F)
                temperature_score = -0.016666666666666666 * (temperature - 95) + 0.25
            elif temperature > 110:
                temperature_score = 0.0
            else:
                temperature_score = 0.0

            # Run the wind algorithm
            if wind_speed < 10:
                wind_score = 2.0
            elif 10 <= wind_speed <= 35:
                # Equation: -0.08(x - 10) + 2.0 (where x is wind speed in MPH)
                wind_score = -0.08 * (wind_speed - 10) + 2.0
            elif wind_speed > 35:
                wind_score = 0.0
            else:
                wind_score = 0.0

            # Run the wind gust algorithm
            wind_gust_difference = wind_gust - wind_speed
            if wind_gust_difference < 7:
                wind_gust_score = 1.0
            elif 7 <= wind_gust_difference <= 15:
                # Equation: -0.125(x - 7) + 1.0 (where x is wind gust difference in MPH)
                wind_gust_score = -0.125 * (wind_gust_difference - 7) + 1.0
            elif wind_gust_difference > 15:
                wind_gust_score = 0.0
            else:
                wind_gust_score = 0.0

            # Calculate precipitation score based on in/hr precip rate.

            if precip_intensity == 0.0:
                precip_score = 5.0
            elif 0.0 < precip_intensity <= 0.05:
                if precip_type == "rain":
                    # Equation: -80.0 * (x - 0.05) + 1 (where x is raw precip rate)
                    precip_score = -80.0 * (precip_intensity - 0.05) + 1
                elif precip_type == "snow" or precip_type == "sleet":
                    # Equation: -100.0 * (x - 0.05) (where x is raw precip rate)
                    precip_score = -100.0 * (precip_intensity - 0.05)
                    # Fix for a negative -0.0, make it 0.0
                    if precip_score == -0.0:
                        precip_score = 0.0
                else:
                    # Equation: -80.0 * (x - 0.05) + 1 (where x is raw precip rate)
                    precip_score = -80.0 * (precip_intensity - 0.05) + 1
            elif 0.05 < precip_intensity <= 0.075:
                if precip_type == "rain":
                    # Equation: -40.0 * (x - 0.075) (where x is raw precip rate)
                    precip_score = -40.0 * (precip_intensity - 0.075)
                    # Fix for a -0.0 score, make it 0.0
                    if precip_score == -0.0:
                        precip_score = 0.0
                elif precip_type == "snow" or precip_type == "sleet":
                    precip_score = 0.0
                else:
                    # Equation: -40.0 * (x - 0.075) (where x is raw precip rate)
                    precip_score = -40.0 * (precip_intensity - 0.075)
                    # Fix for a -0.0 score, make it 0.0
                    if precip_score == -0.0:
                        precip_score = 0.0
            elif precip_intensity > 0.075:
                precip_score = 0.0
            else:
                precip_score = 0.0

            # Calculate visibility score
            if visibility > 5:
                visibility_score = 1.0
            elif 1 <= visibility <= 5:
                # Equation: -0.25(4 - x) + 0.75 (where x is visibility in miles)
                visibility_score = -0.25 * (4 - visibility) + 0.75
            elif visibility < 1:
                visibility_score = 0.0
            else:
                visibility_score = 0.0

            temperature_score = round(temperature_score, 2)
            wind_score = round(wind_score, 2)
            wind_gust_score = round(wind_gust_score, 2)
            precip_score = round(precip_score, 2)
            visibility_score = round(visibility_score, 2)
            wind_gust_difference = round(wind_gust_difference, 2)

            final_score = temperature_score + wind_score + wind_gust_score + precip_score + visibility_score
            final_score = round(final_score, 2)

            # Do some emoji calculations
            good_emoji = "✔"
            caution_emoji = "⚠"
            bad_emoji = "✖"

            # Temperature calculations
            if promode is True:
                if 0.49 <= temperature_score <= 1.0:
                    temperature_emoji = good_emoji
                elif 0.21 <= temperature_score < 0.49:
                    temperature_emoji = caution_emoji
                elif 0.0 <= temperature_score < 0.21:
                    temperature_emoji = bad_emoji
                else:
                    temperature_emoji = "?"
            else:
                if 0.61 <= temperature_score <= 1.0:
                    temperature_emoji = good_emoji
                elif 0.25 <= temperature_score < 0.61:
                    temperature_emoji = caution_emoji
                elif 0.0 <= temperature_score < 0.25:
                    temperature_emoji = bad_emoji
                else:
                    temperature_emoji = "?"

            # Wind calculations
            if promode is True:
                if 1.3 <= wind_score <= 2.0:
                    wind_emoji = good_emoji
                elif 0.5 <= wind_score < 1.3:
                    wind_emoji = caution_emoji
                elif 0.0 <= wind_score < 0.5:
                    wind_emoji = bad_emoji
                else:
                    wind_emoji = "?"
            else:
                if 1.6 <= wind_score <= 2.0:
                    wind_emoji = good_emoji
                elif 0.8 <= wind_score < 1.6:
                    wind_emoji = caution_emoji
                elif 0.0 <= wind_score < 0.8:
                    wind_emoji = bad_emoji
                else:
                    wind_emoji = "?"

            # Wind Gust calculations
            if promode is True:
                if 0.55 <= wind_gust_score <= 1.0:
                    wind_gust_emoji = good_emoji
                elif 0.28 <= wind_gust_score < 0.55:
                    wind_gust_emoji = caution_emoji
                elif 0.0 <= wind_gust_score < 0.28:
                    wind_gust_emoji = bad_emoji
                else:
                    wind_gust_emoji = "?"
            else:
                if 0.8 <= wind_gust_score <= 1.0:
                    wind_gust_emoji = good_emoji
                elif 0.4 <= wind_gust_score < 0.8:
                    wind_gust_emoji = caution_emoji
                elif 0.0 <= wind_gust_score < 0.4:
                    wind_gust_emoji = bad_emoji
                else:
                    wind_gust_emoji = "?"

            # Precip calculations
            if promode is True:
                if 4.50 <= precip_score <= 5.0:
                    precip_emoji = good_emoji
                elif 3.3 <= precip_score < 4.50:
                    precip_emoji = caution_emoji
                elif 0.0 <= precip_score < 3.3:
                    precip_emoji = bad_emoji
                else:
                    precip_emoji = "?"
            else:
                if 5.0 <= precip_score <= 5.0:
                    precip_emoji = good_emoji
                elif 3.5 <= precip_score < 5.0:
                    precip_emoji = caution_emoji
                elif 0.0 <= precip_score < 3.5:
                    precip_emoji = bad_emoji
                else:
                    precip_emoji = "?"

            # Visibility calculations
            if promode is True:
                if 0.55 <= visibility_score <= 1.0:
                    visibility_emoji = good_emoji
                elif 0.25 <= visibility_score < 0.55:
                    visibility_emoji = caution_emoji
                elif 0.0 <= visibility_score < 0.25:
                    visibility_emoji = bad_emoji
                else:
                    visibility_emoji = "?"
            else:
                if 0.75 <= visibility_score <= 1.0:
                    visibility_emoji = good_emoji
                elif 0.5 <= visibility_score < 0.75:
                    visibility_emoji = caution_emoji
                elif 0.0 <= visibility_score < 0.5:
                    visibility_emoji = bad_emoji
                else:
                    visibility_emoji = "?"

            # Calculate how much the score gets subtracted because of alerts
            # Sometimes, a region is on the border of two adjacent alerts, so we store the name of an alert, and if the alert has already
            # been pulled up, continue along.

            # Promode reduces the impact of the score - 0.35 for advisory, 0.75 for watch, and 1.5 for warning.
            alertnames = []
            try:
                for alert in weather_json['alerts']:
                    severity = alert['severity']
                    title = alert['title']
                    expiretime = alert['expires']
                    if time <= expiretime:
                        if title in alertnames:
                            continue
                        else:
                            alertnames.append(title)

                        if promode is True:
                            if severity == "advisory":
                                final_score = final_score - 0.35
                            elif severity == "watch":
                                final_score = final_score - 0.75
                            elif severity == "warning":
                                final_score = final_score - 1.5
                            else:
                                continue
                        else:
                            if severity == "advisory":
                                final_score = final_score - 0.5
                            elif severity == "watch":
                                final_score = final_score - 1.0
                            elif severity == "warning":
                                final_score = final_score - 2.0
                            else:
                                continue

            except KeyError:
                keepgoing = "yes"

            # If the score is below 0.0, bottom it out at 0.0.

            if final_score < 0.0:
                final_score = 0.0

            # Round out the final score again
            final_score = round(final_score, 2)

            # Calculate amount of warnings
            warnings = 0
            if uv_index >= 6.0:
                warnings = warnings + 1

            if humidity >= 0.90:
                warnings = warnings + 1

            if temperature <= 20:
                warnings = warnings + 1

            if temperature >= 100:
                warnings = warnings + 1

            if cloud_cover >= 0.95:
                warnings = warnings + 1

            print("Here's the score for %s at %s:" % (location, datetime.fromtimestamp(time, tz).strftime("%-l:%M %p %Z")))

            if promode is False:
                if 8.5 <= final_score <= 10.0:
                    print(good_emoji + " It will be OK to fly!")
                elif 7.0 <= final_score < 8.5:
                    print(caution_emoji + " It will be OK to fly, but take caution!")
                elif 6.0 <= final_score < 7.0:
                    print(caution_emoji + " It will be OK to fly, but take extreme caution!")
                elif 0.0 <= final_score < 6.0:
                    print(bad_emoji + " It won't be OK to fly.")
                else:
                    print("? We had trouble calculating your score.")
            elif promode is True:
                if 7.5 <= final_score <= 10.0:
                    print(good_emoji + " It will be OK to fly!")
                elif 5.5 <= final_score < 7.5:
                    print(caution_emoji + " It will be OK to fly, but take caution!")
                elif 4.5 <= final_score < 5.5:
                    print(caution_emoji + " It will be OK to fly, but take extreme caution!")
                elif 0.0 <= final_score < 4.5:
                    print(bad_emoji + " It won't be OK to fly.")
                else:
                    print("? We had trouble calculating your score.")

            print("  Overall score: %s/10" % final_score)
            print("")
            alertnames = []
            try:
                for alert in weather_json['alerts']:
                    severity = alert['severity']
                    title = alert['title']
                    expiretime = alert['expires']

                    if time <= expiretime:
                        if title in alertnames:
                            continue
                        else:
                            alertnames.append(title)

                        if promode is True:
                            if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                                print("⚠ A %s will be in effect for your area. 0.35 points subtracted from the total score." % title)
                            elif title.lower().find("watch") == 0:
                                print("⚠ A %s will be in effect for your area. 0.75 points subtracted from the total score." % title)
                            elif title.lower().find("warning") == 0:
                                print("⚠ A %s will be in effect for your area. 1.5 points subtracted from the total score." % title)
                            elif severity == "advisory":
                                print("⚠ A %s will be in effect for your area. 0.35 points subtracted from the total score." % title)
                            elif severity == "watch":
                                print("⚠ A %s will be in effect for your area. 0.75 points subtracted from the total score." % title)
                            elif severity == "warning":
                                print("⚠ A %s will be in effect for your area. 1.5 points subtracted from the total score." % title)
                            else:
                                continue
                        else:
                            if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                                print("⚠ A %s will be in effect for your area. 0.5 points subtracted from the total score." % title)
                            elif title.lower().find("watch") == 0:
                                print("⚠ A %s will be in effect for your area. 1.0 points subtracted from the total score." % title)
                            elif title.lower().find("warning") == 0:
                                print("⚠ A %s will be in effect for your area. 2.0 points subtracted from the total score." % title)
                            elif severity == "advisory":
                                print("⚠ A %s will be in effect for your area. 0.5 points subtracted from the total score." % title)
                            elif severity == "watch":
                                print("⚠ A %s will be in effect for your area. 1.0 points subtracted from the total score." % title)
                            elif severity == "warning":
                                print("⚠ A %s will be in effect for your area. 2.0 points subtracted from the total score." % title)
                            else:
                                continue

            except KeyError:
                keepgoing = "yes"
            print(temperature_emoji + " Temperature score: %s/1.0 (Temp: %s°F)" % (
            temperature_score, temperature))
            print(wind_emoji + " Wind score: %s/2.0 (Wind speed: %s MPH)" % (wind_score, wind_speed))
            print(wind_gust_emoji + " Wind gust score: %s/1.0 (Wind gust: %s MPH | Wind gust difference %s MPH)" %
                  (wind_gust_score, wind_gust, wind_gust_difference))
            print(precip_emoji + " Precipitation score: %s/5.0 (Precip intensity: %s in/hr)" %
                  (precip_score, precip_intensity))
            print(visibility_emoji + " Visibility score: %s/1.0 (Visibility: %s miles)" % (
            visibility_score, visibility))
            if condensedwarnings is True:
                if warnings == 1:
                    print("⚠ There is %s additional warning." % warnings)
                elif warnings >= 2:
                    print("⚠ There are %s additional warnings." % warnings)
            else:
                if 6.0 <= uv_index <= 10.0:
                    print("")
                    print(
                        "⚠ The UV index will be %s. Make sure to apply sunscreen often, and stay in shade if possible." % uv_index)
                elif uv_index > 10.0:
                    print("")
                    print(
                        "⚠ The UV index will be %s. Take extreme caution when in direct sunlight. Make sure to apply" % uv_index,
                        "  sunscreen often, and stay in shade if possible.", sep="\n")

                if humidity >= 0.90:
                    print("")
                    print(
                        "⚠ The humidity level will be %s percent. Water vapor may collect on your drone," % (humidity * 100),
                        "  which could affect internal components & flight performance. Take extra care flying.",
                        sep="\n")

                if temperature <= 20:
                    print("")
                    print(
                        "⚠ The temperature will be %s°F. Before flying, hover your drone for a minute" % temperature,
                        "  to let the battery warm up. Make sure you are wearing a hat & gloves, and stay aware of frostbite.",
                        sep="\n")

                if temperature >= 100:
                    print("")
                    print(
                        "⚠ The temperature will be %s°F. Try to limit flight time to prevent drone overheating." % temperature,
                        "  Make sure you stay hydrated, and limit your time outside to prevent heat stroke.", sep="\n")

                if cloud_cover >= 0.95:
                    print("")
                    print(
                        "⚠ The cloud cover will be %s percent. You may have trouble keeping sight of your drone." % (
                                    cloud_cover * 100),
                        "  If you lose sight, fly back immediately. Additionally, photos & videos could come out poorly.",
                        sep="\n")
            print("----------------------")
            if i < 6:
                print("Press any key to see the next hour of data, press x to exit to the main menu.")
                try:
                    charinput = getch()
                    print("")
                except KeyboardInterrupt:
                    sys.exit()
                except:
                    try:
                        charinput = input("Input here, then press enter: ")
                    except KeyboardInterrupt:
                        sys.exit()
                if charinput == "b'x'" or charinput == "x":
                    break
                else:
                    continue
            else:
                print("End of hourly data.")

    elif charinput == "b'w'" or charinput == "w":
        print("----------------------")

        temperature = weather_json['currently']['temperature']
        precip_intensity = weather_json['currently']['precipIntensity']
        if precip_intensity > 0:
            precip_type = weather_json['currently']['precipType']
        else:
            precip_type = "none"
        wind_speed = weather_json['currently']['windSpeed']
        wind_gust = weather_json['currently']['windGust']
        visibility = weather_json['currently']['visibility']
        conditions = weather_json['currently']['summary']
        uv_index = weather_json['currently']['uvIndex']
        try:
            nearest_storm_distance = weather_json['currently']['nearestStormDistance']
        except KeyError:
            nearest_storm_distance = 999
        humidity = weather_json['currently']['humidity']
        cloud_cover = weather_json['currently']['cloudCover']

        if local_warnings == 0:
            print("There aren't any additional warnings at this time.")
        else:
            print("Here are the additional warnings in your area:")

        if 6.0 <= uv_index <= 10.0:
            print("")
            print(
                "⚠ The current UV index is %s. Make sure to apply sunscreen often, and stay in shade if possible." % uv_index)
        elif uv_index > 10.0:
            print("")
            print(
                "⚠ The current UV index is %s. Take extreme caution when in direct sunlight. Make sure to apply" % uv_index,
                "  sunscreen often, and stay in shade if possible.", sep="\n")

        if humidity >= 0.90:
            print("")
            print("⚠ The humidity level is %s percent. Water vapor may collect on your drone," % (humidity * 100),
                  "  which could affect internal components & flight performance. Take extra care flying.", sep="\n")

        if temperature <= 20:
            print("")
            print("⚠ The current temperature is %s°F. Before flying, hover your drone for a minute" % temperature,
                  "  to let the battery warm up. Make sure you are wearing a hat & gloves, and stay aware of frostbite.",
                  sep="\n")

        if temperature >= 100:
            print("")
            print(
                "⚠ The current temperature is %s°F. Try to limit flight time to prevent drone overheating." % temperature,
                "  Make sure you stay hydrated, and limit your time outside to prevent heat stroke.", sep="\n")

        if cloud_cover >= 0.95:
            print("")
            print("⚠ The current cloud cover is %s percent. You may have trouble keeping sight of your drone." % (
                        cloud_cover * 100),
                  "  If you lose sight, fly back immediately. Additionally, photos & videos could come out poorly.",
                  sep="\n")

        print("")
        print("----------------------")

    elif charinput == "b'a'" or charinput == "a":
        print("----------------------")
        print("OKToFly - Version %s" % version)
        print("Copyright (C) 2019 - 2020 o355.")
        print("Licensed under the GNU GPL v3.")
        print("")
        print("Build number: %s" % buildnumber)
        print("Release date: %s" % releasedate)
        print("Release type: %s" % releasetype)
        print("----------------------")

    elif charinput == "b'c'" or charinput == "c":
        temperature = weather_json['currently']['temperature']
        precip_intensity = weather_json['currently']['precipIntensity']
        if precip_intensity > 0:
            precip_type = weather_json['currently']['precipType']
        else:
            precip_type = "none"
        wind_speed = weather_json['currently']['windSpeed']
        wind_gust = weather_json['currently']['windGust']
        visibility = weather_json['currently']['visibility']
        conditions = weather_json['currently']['summary']
        uv_index = weather_json['currently']['uvIndex']
        try:
            nearest_storm_distance = weather_json['currently']['nearestStormDistance']
        except KeyError:
            nearest_storm_distance = 999
        humidity = weather_json['currently']['humidity']
        cloud_cover = weather_json['currently']['cloudCover']
        timezone = weather_json['timezone']
        tz = pytz.timezone(timezone)

        # Run the temperature algorithm
        if temperature <= 0:
            temperature_score = 0.0
        elif 0 < temperature <= 15:
            # Equation: 0.016666666666666666x (where x is temperature in F)
            temperature_score = 0.016666666666666666 * temperature
        elif 15 < temperature <= 50:
            # Equation: 0.02142857142857143(x - 15) + 0.25 (where x is temperature in F)
            temperature_score = 0.02142857142857143 * (temperature - 15) + 0.25
        elif 50 < temperature <= 80:
            temperature_score = 1.0
        elif 80 < temperature <= 95:
            # Equation: -0.05 * (x - 80) + 1 (where x is temperature)
            temperature_score = -0.05 * (temperature - 80) + 1
        elif 95 < temperature <= 110:
            # Equation: -0.016666666666666666(x - 95) + 0.25 (where x is temperature in F)
            temperature_score = -0.016666666666666666 * (temperature - 95) + 0.25
        elif temperature > 110:
            temperature_score = 0.0
        else:
            temperature_score = 0.0

        # Run the wind algorithm
        if wind_speed < 10:
            wind_score = 2.0
        elif 10 <= wind_speed <= 20:
            # Equation: -0.20(x - 10) + 2.0 (where x is wind speed in MPH)
            wind_score = -0.20 * (wind_speed - 10) + 2.0
        elif wind_speed > 20:
            wind_score = 0.0
        else:
            wind_score = 0.0

        # Run the wind gust algorithm
        wind_gust_difference = wind_gust - wind_speed
        if wind_gust_difference < 7:
            wind_gust_score = 1.0
        elif 7 <= wind_gust_difference <= 15:
            # Equation: -0.125(x - 7) + 1.0 (where x is wind gust difference in MPH)
            wind_gust_score = -0.125 * (wind_gust_difference - 7) + 1.0
        elif wind_gust_difference > 15:
            wind_gust_score = 0.0
        else:
            wind_gust_score = 0.0

        # Calculate precipitation score based on in/hr precip rate.

        if precip_intensity == 0.0:
            precip_score = 5.0
        elif 0.0 < precip_intensity <= 0.05:
            if precip_type == "rain":
                # Equation: -80.0 * (x - 0.05) + 1 (where x is raw precip rate)
                precip_score = -80.0 * (precip_intensity - 0.05) + 1
            elif precip_type == "snow" or precip_type == "sleet":
                # Equation: -100.0 * (x - 0.05) (where x is raw precip rate)
                precip_score = -100.0 * (precip_intensity - 0.05)
                # Fix for a negative -0.0, make it 0.0
                if precip_score == -0.0:
                    precip_score = 0.0
            else:
                # Equation: -80.0 * (x - 0.05) + 1 (where x is raw precip rate)
                precip_score = -80.0 * (precip_intensity - 0.05) + 1
        elif 0.05 < precip_intensity <= 0.075:
            if precip_type == "rain":
                # Equation: -40.0 * (x - 0.075) (where x is raw precip rate)
                precip_score = -40.0 * (precip_intensity - 0.075)
                # Fix for a -0.0 score, make it 0.0
                if precip_score == -0.0:
                    precip_score = 0.0
            elif precip_type == "snow" or precip_type == "sleet":
                precip_score = 0.0
            else:
                # Equation: -40.0 * (x - 0.075) (where x is raw precip rate)
                precip_score = -40.0 * (precip_intensity - 0.075)
                # Fix for a -0.0 score, make it 0.0
                if precip_score == -0.0:
                    precip_score = 0.0
        elif precip_intensity > 0.075:
            precip_score = 0.0
        else:
            precip_score = 0.0

        # Calculate visibility score
        if visibility > 5:
            visibility_score = 1.0
        elif 1 <= visibility <= 5:
            # Equation: -0.25(4 - x) + 0.75 (where x is visibility in miles)
            visibility_score = -0.25 * (4 - visibility) + 0.75
        elif visibility < 1:
            visibility_score = 0.0
        else:
            visibility_score = 0.0

        temperature_score = round(temperature_score, 2)
        wind_score = round(wind_score, 2)
        wind_gust_score = round(wind_gust_score, 2)
        precip_score = round(precip_score, 2)
        visibility_score = round(visibility_score, 2)
        wind_gust_difference = round(wind_gust_difference, 2)

        final_score = temperature_score + wind_score + wind_gust_score + precip_score + visibility_score
        final_score = round(final_score, 2)

        # Do some emoji calculations
        good_emoji = "✔"
        caution_emoji = "⚠"
        bad_emoji = "✖"

        # Temperature calculations
        if promode is True:
            if 0.49 <= temperature_score <= 1.0:
                temperature_emoji = good_emoji
            elif 0.21 <= temperature_score < 0.49:
                temperature_emoji = caution_emoji
            elif 0.0 <= temperature_score < 0.21:
                temperature_emoji = bad_emoji
            else:
                temperature_emoji = "?"
        else:
            if 0.61 <= temperature_score <= 1.0:
                temperature_emoji = good_emoji
            elif 0.25 <= temperature_score < 0.61:
                temperature_emoji = caution_emoji
            elif 0.0 <= temperature_score < 0.25:
                temperature_emoji = bad_emoji
            else:
                temperature_emoji = "?"

        # Wind calculations
        if promode is True:
            if 1.3 <= wind_score <= 2.0:
                wind_emoji = good_emoji
            elif 0.5 <= wind_score < 1.3:
                wind_emoji = caution_emoji
            elif 0.0 <= wind_score < 0.5:
                wind_emoji = bad_emoji
            else:
                wind_emoji = "?"
        else:
            if 1.6 <= wind_score <= 2.0:
                wind_emoji = good_emoji
            elif 0.8 <= wind_score < 1.6:
                wind_emoji = caution_emoji
            elif 0.0 <= wind_score < 0.8:
                wind_emoji = bad_emoji
            else:
                wind_emoji = "?"

        # Wind Gust calculations
        if promode is True:
            if 0.55 <= wind_gust_score <= 1.0:
                wind_gust_emoji = good_emoji
            elif 0.28 <= wind_gust_score < 0.55:
                wind_gust_emoji = caution_emoji
            elif 0.0 <= wind_gust_score < 0.28:
                wind_gust_emoji = bad_emoji
            else:
                wind_gust_emoji = "?"
        else:
            if 0.8 <= wind_gust_score <= 1.0:
                wind_gust_emoji = good_emoji
            elif 0.4 <= wind_gust_score < 0.8:
                wind_gust_emoji = caution_emoji
            elif 0.0 <= wind_gust_score < 0.4:
                wind_gust_emoji = bad_emoji
            else:
                wind_gust_emoji = "?"

        # Precip calculations
        if promode is True:
            if 4.50 <= precip_score <= 5.0:
                precip_emoji = good_emoji
            elif 3.3 <= precip_score < 4.50:
                precip_emoji = caution_emoji
            elif 0.0 <= precip_score < 3.3:
                precip_emoji = bad_emoji
            else:
                precip_emoji = "?"
        else:
            if 5.0 <= precip_score <= 5.0:
                precip_emoji = good_emoji
            elif 3.5 <= precip_score < 5.0:
                precip_emoji = caution_emoji
            elif 0.0 <= precip_score < 3.5:
                precip_emoji = bad_emoji
            else:
                precip_emoji = "?"

        # Visibility calculations
        if promode is True:
            if 0.55 <= visibility_score <= 1.0:
                visibility_emoji = good_emoji
            elif 0.25 <= visibility_score < 0.55:
                visibility_emoji = caution_emoji
            elif 0.0 <= visibility_score < 0.25:
                visibility_emoji = bad_emoji
            else:
                visibility_emoji = "?"
        else:
            if 0.75 <= visibility_score <= 1.0:
                visibility_emoji = good_emoji
            elif 0.5 <= visibility_score < 0.75:
                visibility_emoji = caution_emoji
            elif 0.0 <= visibility_score < 0.5:
                visibility_emoji = bad_emoji
            else:
                visibility_emoji = "?"

        # Calculate how much the score gets subtracted because of alerts
        # Sometimes, a region is on the border of two adjacent alerts, so we store the name of an alert, and if the alert has already
        # been pulled up, continue along.

        # Promode reduces the impact of the score - 0.35 for advisory, 0.75 for watch, and 1.5 for warning.
        alertnames = []
        try:
            for alert in weather_json['alerts']:
                severity = alert['severity']
                title = alert['title']
                if title in alertnames:
                    continue
                else:
                    alertnames.append(title)

                if promode is True:
                    if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                        final_score = final_score - 0.35
                    elif title.lower().find("watch") == 0:
                        final_score = final_score - 0.75
                    elif title.lower().find("warning") == 0:
                        final_score = final_score - 1.5
                    elif severity == "advisory":
                        final_score = final_score - 0.35
                    elif severity == "watch":
                        final_score = final_score - 0.75
                    elif severity == "warning":
                        final_score = final_score - 1.5
                    else:
                        continue
                else:
                    if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                        final_score = final_score - 0.5
                    elif title.lower().find("watch") == 0:
                        final_score = final_score - 1.0
                    elif title.lower().find("warning") == 0:
                        final_score = final_score - 2.0
                    elif severity == "advisory":
                        final_score = final_score - 0.5
                    elif severity == "watch":
                        final_score = final_score - 1.0
                    elif severity == "warning":
                        final_score = final_score - 2.0
                    else:
                        continue

        except KeyError:
            keeping = "yes"

        # If the score is below 0.0, bottom it out at 0.0.

        if final_score < 0.0:
            final_score = 0.0

        # Round out the final score again
        final_score = round(final_score, 2)

        # Calculate amount of warnings
        warnings = 0
        if uv_index >= 6.0:
            warnings = warnings + 1

        if humidity >= 0.90:
            warnings = warnings + 1

        if temperature <= 20:
            warnings = warnings + 1

        if temperature >= 100:
            warnings = warnings + 1

        if cloud_cover >= 0.95:
            warnings = warnings + 1

        local_warnings = warnings

        time = weather_json['currently']['time']

        print("----------------------")
        print("Here's the score for %s at %s:" % (location, datetime.fromtimestamp(time, tz).strftime("%-l:%M %p %Z")))

        if promode is True:
            if 7.5 <= final_score <= 10.0:
                print(good_emoji + " It's OK to fly!")
            elif 5.5 <= final_score < 7.5:
                print(caution_emoji + " It's OK to fly, but take caution!")
            elif 4.5 <= final_score < 5.5:
                print(caution_emoji + " It's OK to fly, but take extreme caution!")
            elif 0.0 <= final_score < 4.5:
                print(bad_emoji + " It's not OK to fly.")
            else:
                print("? We had trouble calculating your score.")
        else:
            if 8.5 <= final_score <= 10.0:
                print(good_emoji + " It's OK to fly!")
            elif 7.0 <= final_score < 8.5:
                print(caution_emoji + " It's OK to fly, but take caution!")
            elif 6.0 <= final_score < 7.0:
                print(caution_emoji + " It's OK to fly, but take extreme caution!")
            elif 0.0 <= final_score < 6.0:
                print(bad_emoji + " It's not OK to fly.")
            else:
                print("? We had trouble calculating your score.")

        print("  Overall score: %s/10" % final_score)
        if promode is True:
            print("  Pro mode is enabled.")
        print("")
        alertnames = []
        try:
            for alert in weather_json['alerts']:
                severity = alert['severity']
                title = alert['title']
                if title in alertnames:
                    continue
                else:
                    alertnames.append(title)

                if promode is True:
                    if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                        print(
                            "⚠ There's a %s in effect for your area. 0.35 points subtracted from the total score." % title)
                    elif title.lower().find("watch") == 0:
                        print(
                            "⚠ There's a %s in effect for your area. 0.75 points subtracted from the total score." % title)
                    elif title.lower().find("warning") == 0:
                        print(
                            "⚠ There's a %s in effect for your area. 1.5 points subtracted from the total score." % title)
                    elif severity == "advisory":
                        print(
                            "⚠ There's a %s in effect for your area. 0.35 points subtracted from the total score." % title)
                    elif severity == "watch":
                        print(
                            "⚠ There's a %s in effect for your area. 0.75 points subtracted from the total score." % title)
                    elif severity == "warning":
                        print(
                            "⚠ There's a %s in effect for your area. 1.5 points subtracted from the total score." % title)
                    else:
                        continue
                else:
                    if title.lower().find("advisory") == 0 or title.lower().find("statement") == 0:
                        print(
                            "⚠ There's a %s in effect for your area. 0.5 points subtracted from the total score." % title)
                    elif title.lower().find("watch") == 0:
                        print(
                            "⚠ There's a %s in effect for your area. 1.0 points subtracted from the total score." % title)
                    elif title.lower().find("warning") == 0:
                        print(
                            "⚠ There's a %s in effect for your area. 2.0 points subtracted from the total score." % title)
                    elif severity == "advisory":
                        print(
                            "⚠ There's a %s in effect for your area. 0.5 points subtracted from the total score." % title)
                    elif severity == "watch":
                        print(
                            "⚠ There's a %s in effect for your area. 1.0 points subtracted from the total score." % title)
                    elif severity == "warning":
                        print(
                            "⚠ There's a %s in effect for your area. 2.0 points subtracted from the total score." % title)
                    else:
                        continue

        except KeyError:
            keepgoing = "yes"

        print(temperature_emoji + " Temperature score: %s/1.0 (Current temp: %s°F)" % (temperature_score, temperature))
        print(wind_emoji + " Wind score: %s/2.0 (Current wind: %s MPH)" % (wind_score, wind_speed))
        print(wind_gust_emoji + " Wind gust score: %s/1.0 (Current wind gust: %s MPH, gust difference %s MPH)" %
              (wind_gust_score, wind_gust, wind_gust_difference))
        print(precip_emoji + " Precipitation score: %s/5.0 (Current precip intensity: %s in/hr)" %
              (precip_score, precip_intensity))
        print(visibility_emoji + " Visibility score: %s/1.0 (Current visibility: %s miles)" % (
        visibility_score, visibility))
        if condensedwarnings is True:
            if warnings == 1:
                print("⚠ There is %s additional warning. Enter w to get more info." % warnings)
            elif warnings >= 2:
                print("⚠ There are %s additional warnings. Enter w to get more info." % warnings)
        else:
            if 6.0 <= uv_index <= 10.0:
                print("")
                print(
                    "⚠ The current UV index is %s. Make sure to apply sunscreen often, and stay in shade if possible." % uv_index)
            elif uv_index > 10.0:
                print("")
                print(
                    "⚠ The current UV index is %s. Take extreme caution when in direct sunlight. Make sure to apply" % uv_index,
                    "  sunscreen often, and stay in shade if possible.", sep="\n")

            if humidity >= 0.90:
                print("")
                print("⚠ The humidity level is %s percent. Water vapor may collect on your drone," % (humidity * 100),
                      "  which could affect internal components & flight performance. Take extra care flying.",
                      sep="\n")

            if temperature <= 20:
                print("")
                print("⚠ The current temperature is %s°F. Before flying, hover your drone for a minute" % temperature,
                      "  to let the battery warm up. Make sure you are wearing a hat & gloves, and stay aware of frostbite.",
                      sep="\n")

            if temperature >= 100:
                print("")
                print(
                    "⚠ The current temperature is %s°F. Try to limit flight time to prevent drone overheating." % temperature,
                    "  Make sure you stay hydrated, and limit your time outside to prevent heat stroke.", sep="\n")

            if cloud_cover >= 0.95:
                print("")
                print("⚠ The current cloud cover is %s percent. You may have trouble keeping sight of your drone." % (
                            cloud_cover * 100),
                      "  If you lose sight, fly back immediately. Additionally, photos & videos could come out poorly.",
                      sep="\n")

        print("----------------------")
    else:
        sys.exit()

