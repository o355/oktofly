# OKToFly Updater API
This is the static API that OKToFly uses to check for updates.

The current version of the API is v1.0.0.

You can query the API by using this url: https://gitlab.com/o355/oktofly/-/raw/master/updater/versioncheck.json

# Documentation
The API is static and simple.

## updater section
The main section of the API response.

### apiversion
The version of the API.

Example: `"v1.0.0"`

Type: `string`

### latestbuild
The latest build number of OKToFly. When using this API, note that this will be an integer as a string.

Example: `"110"`

Type: `integer as a string`

### latestversion
The latest version of OKToFly. This does not have the v character prefixed. Use the latestversion-withv key if you are looking for the v character to be prefixed.

Example: `"1.1.0"`

Type: `string`

### latestversion-withv
The latest version of OKToFly, but with the v prefixed.

Example: `"v1.1.0"`

Type: `string`

### latesturl
The URL to download the latest version of OKToFly.

Example: `"https://gitlab.com/o355/someurl.zip"`

Type: `string`

### releasedate
The release date of the latest version of OKToFly.

Example: `"March 17, 2020"`

Type: `string`

### size
The approximate size of the .zip file for the latest version of OKToFly.

Example: `"~30 KB"`

Type: `string`